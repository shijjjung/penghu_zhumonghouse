const headerImageId = "#bannerImage";
const bgBannerImageID = "#bnbPcBanner";
const prefixImageLocation = 'images/';
let lastImagePath;
const images = [
    'ev_1.jpg', '301/D85_3267_c.jpg', '301/D85_3265_c.jpg', '301/D85_3259_c.jpg', '301/D85_3260_c.jpg', '301/D85_3273_c.jpg', '301/D85_3268_c.jpg', '301/D85_3269_c.jpg', '301/D85_3270_c.jpg', '301/D85_3272_c.jpg', 'public_space/D85_3291_c.jpg', 'public_space/D85_3293_c.jpg', 'public_space/D85_3297_c.jpg', 'public_space/D85_3295_c.jpg', 'public_space/D85_3274_c.jpg', 'public_space/D85_3352_c.jpg', '203/D85_3281_c.jpg', '203/D85_3285_c.jpg', '203/D85_3287_c.jpg', '203/D85_3286_c.jpg', '203/D85_3284_c.jpg', '203/D85_3279_c.jpg', '203/D85_3276_c.jpg', '203/D85_3289_c.jpg', '203/D85_3350_c.jpg', '202/D85_3324_C.jpg', '202/D85_3319_c.jpg', '202/D85_3303_c.jpg', '202/D85_3327_c.jpg', '202/D85_3307_c.jpg', '202/D85_3305_c.jpg', '202/D85_3309_c.jpg', '202/D85_3312_c.jpg', '202/D85_3313_c.jpg', '202/D85_3315_c.jpg', '302/D85_3243_c.jpg', '302/D85_3245_c.jpg', '302/D85_3247_c.jpg', '302/D85_3246_c.jpg', '302/D85_3244_c.jpg', '302/D85_3248_c.jpg', '302/D85_3253_c.jpg', '302/D85_3257_c.jpg', '302/D85_3255_c.jpg', '302/D85_3252_c.jpg', '302/D85_3250_c.jpg', '201/D85_3347_C.jpg', '201/D85_3345_c.jpg', '201/D85_3339_c.jpg', '201/D85_3343_c.jpg', '201/D85_3340_c.jpg', '201/D85_3334_c.jpg', '201/D85_3348_c.jpg', '201/D85_3337_c.jpg', '201/D85_3335_c.jpg', '201/D85_3331_c.jpg'
]
const publicImage = ['public_space/D85_3291_c.jpg', 'public_space/D85_3293_c.jpg', 'public_space/D85_3297_c.jpg', 'public_space/D85_3295_c.jpg', 'public_space/D85_3274_c.jpg', 'public_space/D85_3352_c.jpg']
const r301 = ['301/D85_3267_c.jpg', '301/D85_3265_c.jpg', '301/D85_3259_c.jpg', '301/D85_3260_c.jpg', '301/D85_3273_c.jpg', '301/D85_3268_c.jpg', '301/D85_3269_c.jpg', '301/D85_3270_c.jpg', '301/D85_3272_c.jpg'];
const r302 = ['302/D85_3243_c.jpg', '302/D85_3245_c.jpg', '302/D85_3247_c.jpg', '302/D85_3246_c.jpg', '302/D85_3244_c.jpg', '302/D85_3248_c.jpg', '302/D85_3253_c.jpg', '302/D85_3257_c.jpg', '302/D85_3255_c.jpg', '302/D85_3252_c.jpg', '302/D85_3250_c.jpg'];
const r201 = ['201/D85_3347_C.jpg', '201/D85_3345_c.jpg', '201/D85_3339_c.jpg', '201/D85_3343_c.jpg', '201/D85_3340_c.jpg', '201/D85_3334_c.jpg', '201/D85_3348_c.jpg', '201/D85_3337_c.jpg', '201/D85_3335_c.jpg', '201/D85_3331_c.jpg'];
const r202 = ['202/D85_3324_C.jpg', '202/D85_3319_c.jpg', '202/D85_3303_c.jpg', '202/D85_3327_c.jpg', '202/D85_3307_c.jpg', '202/D85_3305_c.jpg', '202/D85_3309_c.jpg', '202/D85_3312_c.jpg', '202/D85_3313_c.jpg', '202/D85_3315_c.jpg'];
const r203 = ['203/D85_3281_c.jpg', '203/D85_3285_c.jpg', '203/D85_3287_c.jpg', '203/D85_3286_c.jpg', '203/D85_3284_c.jpg', '203/D85_3279_c.jpg', '203/D85_3276_c.jpg', '203/D85_3289_c.jpg', '203/D85_3350_c.jpg',];
const rtype = [r201[0], r202[0], r203[0], r301[0], r302[0]];
const rooms = [r301, r302, r201, r202, r203];
const slideType = {
    room: 0,
    public: 1,
}

let currentType = null;
function initRoomSlideData(e){
    if (currentType == slideType.room) return;
    $("#sj-tab-build").removeClass("active");
    $(this).toggleClass("active");
    let roomBtnsDiv = $(".room-btns");
    roomBtnsDiv.empty();
    currentType = slideType.room;
    for(let i = 0; i <rtype.length; i++){
        let div = $('<a />', {
            'data-lightbox': "image1"+(i+1), 
            'data-title':publicImage[i].split('/')[0], 
            'href':prefixImageLocation + rtype[i]
        }).appendTo(roomBtnsDiv);
        let img = $('<img />',{ 
            'alt': ("房型 " + rtype[i].split('/')[0]),
            'src': prefixImageLocation + rtype[i]}).appendTo(div);
            $( "<div/>", {'class': 'info'}).appendTo(div).append($( "<div/>").text(rtype[i].split('/')[0]));
    }
}

function initPublicSlideData(e){
    if (currentType == slideType.public) return;
    currentType = slideType.public;
    $("#sj-tab-room").removeClass("active");
    $(this).toggleClass("active");
    let roomBtnsDiv = $(".room-btns");
    roomBtnsDiv.empty();
    for(let i = 0; i <publicImage.length; i++){
        // let div = $( "<div/>", {}).appendTo(roomBtnsDiv);
        let div = $('<a />', {
            'data-lightbox': "image1"+(i+1), 
            'data-title':publicImage[i].split('/')[0], 
            'href':prefixImageLocation + publicImage[i]
        }).appendTo(roomBtnsDiv);
        let img = $('<img />',{ 
            'alt': ("房型 " + publicImage[i].split('/')[0]),
            'src': prefixImageLocation + publicImage[i]}).appendTo(div);
            $( "<div/>", {'class': 'info'}).appendTo(div).append($( "<div/>").text("民宿照片"+ (i+1)));
    }
}
let imageTimeOut;
function changeEventImageTicker(){
    lastImagePath = $(headerImageId).css('background-image').split("/images/")[1].replace(/.*\s?url\([\'\"]?/, '').replace(/[\'\"]?\).*/, '');
    imageTimeOut = setTimeout(fadeOutImage, 3000);
}
function fadeInImage(){
    $(headerImageId).fadeTo(1000, 1, changeEventImageTicker);
}
function fadeOutImage(){
    $(bgBannerImageID).css('background-image', 'url(' + prefixImageLocation + lastImagePath + ')');
    clearTimeout(imageTimeOut);
    $(headerImageId).fadeTo(1000, 0, onChangeImage);
}

function getRandom(length){
    const random = Math.floor(Math.random() * length);
    return random
}

function onChangeImage(){
    let random = getRandom(images.length);
    // console.log(images[random]);
    let imageUrl = images[random];
    $(headerImageId).css('background-image', 'url(' + 'images/' + imageUrl + ')');
    fadeInImage();
}

const bnbIntroPhotosSliderID = "#bnbIntroPhotosSlider";
function initPublicImage(){
    for (let i = 0; i< publicImage.length; i++){
        let divInfo = "<div class='img-item js-item'><img class='owl-lazy' data-src=images/" + publicImage[i] + " alt='民宿'></div>"
        $(bnbIntroPhotosSliderID).append(divInfo);
    }
}
const bnbRoomsSliderID = "#bnbRoomsSlider";
function initRoomImages(){
    for(let i = 0; i< rooms.length; i++){
        
        createImgItem("images/" + rooms[i][1], rooms[i][1].split("/")[0]);
    }
}

function createImgItem(imgSrc, txt){
    let div = $( "<div/>", {
        "class": "img-item",
    });
    let img = $('<img />',{ class:'owl-lazy', id: 'Myid',
                'data-src': imgSrc}).appendTo(div);
    $('<a />', {'class': "room-link-blk", 'href':'#'}).appendTo(div);
    $( "<div/>", {
        "class": "room-inner-top",
    }).appendTo(div);
    $( "<div/>", {
        "class": "room-head",
    }).text(txt || "").appendTo(div);
    $( "<div/>", {
        "class": "room-inner-bot",
    }).appendTo(div);
    div.appendTo($(bnbRoomsSliderID));
}

function initTipsButton(){
    $(".t3").click(function(e) {
        e.preventDefault();
        $(this).parent().find('.news-msg').toggleClass("newmsg-hide");
        $(this).toggleClass("t3-up");
    });
    $(".menu-icon-link").click(function(e){
        e.preventDefault();
        $(".topMenu").toggleClass("topMenu-active");
    })
    $("#prev-btn").click(function(e){
        e.preventDefault();
        slideChild(false);
    })
    $("#next-btn").click(function(e){
        e.preventDefault();
        slideChild(true);
    })
}
let lastViewIndex = 0;
let resizeTimer;

function resizefunc(){
    document.body.classList.remove("resize-animation-stopper");
}
window.onresize = function(){
    document.body.classList.add("resize-animation-stopper");
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(resizefunc, 400);

    let perViewAmount = $(window).width() <= 768? 1:3;
    let childWidth = $('.room-btns').children().eq(0).width();
    lastViewIndex = Math.floor(lastViewIndex / perViewAmount);
    let currentRight = Math.floor(lastViewIndex / perViewAmount) * childWidth;
    currentRight = currentRight - (currentRight % childWidth);
    $('.room-btns').animate({
        'right' : currentRight
    }, 0);
};

function clamp(val, min, max){
    return Math.min(Math.max(min, val), max)
}

function slideChild(next){
    let dir = next?1:-1;
    let perViewAmount = $(window).width() <= 768? 1:3;
    let childWidth = $('.room-btns').children().eq(0).width();
    let maxRight = ($(".room-btns").children().length - perViewAmount) * childWidth ;
    let lastRight = parseInt($('.room-btns').css('right'), 10)
    let currentRight = lastRight + dir * childWidth;
    if (maxRight < currentRight){
        currentRight = maxRight;
    }else if (currentRight< 0){
        currentRight = 0;
    }
    else{
        lastViewIndex = clamp(lastViewIndex + (dir), 0, $(".room-btns").children().length - 1);
    }
    $('.room-btns').animate({
        'right' : currentRight   
    }); 
    // console.log($('.room-btns').children().eq(0).width())
    // console.log($('.room-btns').position())
    // console.log($('.room-btns').children().eq(0).position())
}
const mapTitles = [
    "page-home",
    "page-room",
    // "page-sale",
    "page-question",
    "close-spots",
    "page-howtogo",
];
const mapTitleIndex = {
    "#home":0,
    "#room": 1,
    // "#sales": 2,
    "#question": 2,
    "#spots":3,
    "#travel":4,
}
$(window).on('hashchange', function(e){
    onHashChange();
});
function onHashChange(){
    let active_Index;
    // mapTitles[mapTitleIndex[window.location.hash]]
    
    switch (window.location.hash){
        case "#travel":
            active_Index = mapTitleIndex[window.location.hash];
            break
        case "#spots":
            active_Index = mapTitleIndex[window.location.hash];
            break
        case "#question":
            active_Index = mapTitleIndex[window.location.hash];
            break
        case "#room":
            active_Index = mapTitleIndex[window.location.hash];
            break;
        default:
            active_Index = mapTitleIndex["#home"];
            
            break
    }
    for(let i=0; i<mapTitles.length; i++){
        if (i == active_Index){
            $("." + mapTitles[i]).show();
            $(".topMenu").children().eq(i + 1).addClass("active");
        }else{
            $("." + mapTitles[i]).hide();
            $(".topMenu").children().eq(i + 1).removeClass("active");
        }
    }
}
$( document ).ready(function(){
    changeEventImageTicker();
    initRoomImages();
    initPublicImage();
    initTipsButton();
    onHashChange();
    initRoomSlideData();

    $("#sj-tab-room").click(initRoomSlideData);
    $("#sj-tab-build").click(initPublicSlideData);
    // console.log(window.location.hash)
    $(".menu_icon").on("click", function (e) {
        $(".menu").slideToggle();
    });

    $(".favorite").on("click", function (e) {
        e.preventDefault();
        $(this).toggleClass("active");
    });

    
})

