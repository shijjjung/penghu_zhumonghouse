let jsonUrl = "https://shijjjung.gitlab.io/penghu_zhumonghouse/indexContent.json";
$.ajax({
    url: jsonUrl,
    dataType: "json"
}).done(function(result){
    var navButton = document.querySelector("#navButton");
    // console.log(result[0].NavButton)
    let navBtnInfo = result[0].NavButton;
    var navButtonLen = navBtnInfo.length;
    
    for (let i = 0; i<navButtonLen; i++){
        $("#navButton").children().eq(i).attr("href", navBtnInfo[i].href);
        $("#navButton").children().eq(i).find("span" ).text(navBtnInfo[i].content);
    }
    // console.log(navButtonLen);
    initScenePanelGrid(result[0].Scene);
});

function initScenePanelGrid(params) {
    // console.log(params)
    for (i = 0; i < params.length; i++) {
        let div = $( "<div/>", {
            "class": "panel-body panel-primary",
        });
        div.appendTo($("#scene"));
        let a = $('<a />', {'class': "spots-grid", 'href':params[i].href})
        a.appendTo(div);
        let div_sc_img = $( "<div/>", {
            "class": "sc_img",
        }).appendTo(a);
        // console.log(params[i].name);
        let img = $('<img />',{ 
            'alt': params[i].name, 
            'src': params[i].img
        }).appendTo(div_sc_img);
        let div_sc_content = $( "<div/>", {
            "class": "sc_content",
        }).appendTo(a);
        $( "<div/>", {}).text(params[i].name).appendTo(div_sc_content);
        $( "<div/>", {}).html(params[i].content).appendTo(div_sc_content);
      }
}
