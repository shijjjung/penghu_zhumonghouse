/* update time: 2018-03-30 */
var Tools = (function(window) {

    var nativeCeil = Math.ceil,
        nativeFloor = Math.floor,
        nativeMax = Math.max,
        nativeMin = Math.min,
        nativeNow = Date.now,
        nativeRandom = Math.random;

    /**
     * [隨機取得 min ~ max 間的數值(含小數點)]
     * @param  {Number} min 
     * @param  {Number} max 
     * @return {Number}    
     */
    var randomRange = function(min, max) {
        return min + nativeRandom() * (max - min);
    };

    /**
     * [隨機取得 min ~ max 間的數值(整數)]
     * @param  {Number} min 
     * @param  {Number} max 
     * @return {Number}    
     */
    var randomInt = function(min, max) {
        return nativeFloor(min + nativeRandom() * (max - min + 1));
    };

    /**
     * [載入陣列裡所有的圖片後執行callback]
     * @param  {Array} imgArray 
     * @param  {function} callback   
     */
    var preload = function(imgArr, callback) {
        var imgs = [];
        var loadCnt = 0;

        imgArr.forEach(function(obj, i) {
            imgs[i] = new Image();
            imgs[i].onload = function() {
                // console.log('image ' + i + ' is on load!');
                loadCnt += 1;

                if (loadCnt === imgArr.length) {
                    // 最後一張 (這張載完才可以開始玩);
                    callback(imgs);
                };

            };
            imgs[i].src = obj.src;
        });
    };

    /**
     * [取得 URL 所有參數]
     * @return {Object}      網址參數
     */
    var getUrlVars = function() {
        var vars = {},
            hash;
        var hashes = window.location.search.slice(window.location.search.indexOf('?') + 1).split('&'); //改這寫法才不會取到#
        for (var i = 0, len = hashes.length; i < len; i++) {
            hash = hashes[i].split('=');
            vars[hash[0]] = hash[1];
        }
        return vars;
    };

    /**
     * 取得 URL 指定參數的值
     * @param  {String} name 網址參數 name
     * @return {String}      網址參數 value
     */
    var getUrlVar = function(name) {
        return getUrlVars()[name];
    };

    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    /**
     * 判斷是否為行動裝置
     * @type {Boolean}
     */
    var isMobile = userAgent.match(/iPhone/i) || userAgent.match(/iPod/i) || userAgent.match(/Android/i) ? true : false;

    var isAndroid = userAgent.match(/Android/i) ? true : false;

    /**
     * 行動裝置的 touch 事件字串，若不是行動裝置，取得 mouse 事件字串
     */
    var Event = (function() {
        if (isMobile) {
            return {
                down: 'touchstart',
                move: 'touchmove',
                up: 'touchend'
            }
        } else {
            return {
                down: 'mousedown',
                move: 'mousemove',
                up: 'mouseup'
            };
        };
    })();

    /**
     * 打亂陣列順序
     * @param {Array} 指定的陣列
     * @param {number} array 的長度 可傳可不傳
     * @return {Array} 
     */
    var shuffleSelf = function(array, size) {
        var index = -1,
            length = array.length,
            lastIndex = length - 1;

        size = size === undefined ? length : size;
        while (++index < size) {
            var rand = randomInt(index, lastIndex),
                value = array[rand];

            array[rand] = array[index];
            array[index] = value;
        }
        array.length = size;
        return array;
    };

    /**
     * 組裝DOM
     * @param {strId} 指定字串
     * @param {obj} 物件名稱
     */
    function setDOM(strId, obj) {
        var elm = document.getElementById(strId);
        for (var i = obj[strId].length - 1; i >= 0; i--) {
            elm.insertBefore(document.getElementById(obj[strId][i]), elm.firstChild);
        };
    };

    /**
     * 刪除DOM
     * @param {arr} 指定陣列
     */
    function delDOM(arr) { // 刪除指定的DOM
        arr.forEach(function(val) {
            var node = document.getElementById(val);
            if (node.parentNode) {
                node.parentNode.removeChild(node);
            };
        });
    };

    /**
     * 刪除DOM
     * @param {id} 指定DOM id
     */
    function delDOMSingle(id) { // 刪除指定的DOM
        var node = document.getElementById(id);
        if (node.parentNode) {
            node.parentNode.removeChild(node);
        };
    };


    var winBody = (function() {
        var ua = navigator.userAgent.toLowerCase()
        if (ua.match(/chrome/i) || ua.match(/firefox/i)) {
            return 'documentElement';
        } else if (ua.match(/safari/i)) {
            return 'body';
        } else {
            return 'documentElement';
        };
    })();

    return {
        preload: preload,
        randomRange: randomRange,
        randomInt: randomInt,
        getUrlVars: getUrlVars,
        getUrlVar: getUrlVar,
        isMobile: isMobile,
        isAndroid: isAndroid,
        Event: Event,
        shuffleSelf: shuffleSelf,
        setDOM: setDOM,
        delDOM: delDOM,
        winBody: winBody,
        delDOMSingle: delDOMSingle
    };

})(window);